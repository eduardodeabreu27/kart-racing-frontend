//import { environment } from "../../environments/environment";
import * as io from 'socket.io-client';

/**!
 * nodocms backend application
 * Copyright(c) 2015 Johannes Pichler - webpixels
 * Created by johannespichler on 23.11.15.
 * LGPLv3.0 Licensed
 **/




/**
    we declare io here for typescript. io is a global window object
    you have to provide socket.io in your html markup for your application like:
    <script src="socket.io/socket.io.js"></script>
    or
    <script src="path/to/my/socket.io.js"></script>
 */

/**
 * the SocketService Object Class
 */
export class SocketService {

   /*  static instance:SocketService = null;
    static isCreating:Boolean = false;
    socket:any; */

    /**
     * constuctor with control handle, that you can not instantiate by new SocketService();
     * socket should act as a real singleton, not to have multiple socket connection instances
     * within the same application to the same resource
     */
    /* constructor() {

        if (!SocketService.isCreating) {
            throw new Error("This is a real singleton. Get an instance via var socket = SocketService.getInsance(); !");
        }

        console.info("creating socket object");

        this.socket = io(environment.BASE_URL_SOCKET_SERVICE + environment.PORT_SOCKET_SERVICE);
        console.info("establishing connection to server...");
        this.socket.connect();
    } */


    /**
     * receive data form Socket-Server
     * @param eventName
     * @param callback
     */
   /*  public on(eventName, callback):void {

        this.socket.on(eventName, function () {
            var args = arguments;
            if (typeof callback == "function") {
                callback.apply(this.socket, args);
            }
        });
    } */

    /**
     * submit data to socket-server
     * @param eventName
     * @param data
     * @param callback
     */
   /*  public emit(eventName, data, callback):void {
        this.socket.emit(eventName, data, function () {
            var args = arguments;
            if (typeof callback == "function") {
                callback.apply(this.socket, args);

            }
        });
    } */




    /**
     * get instance wrapper
     * @returns {SocketService}
     */
   /*  static getInstance():SocketService {

        if (SocketService.instance === null) {
            SocketService.isCreating = true;
            SocketService.instance = new SocketService();
            SocketService.isCreating = false;
        }

        console.log("SocketService.instance",SocketService.instance);
        return SocketService.instance;

    } */

}