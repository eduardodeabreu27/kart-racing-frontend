import {Injectable} from '@angular/core';
import {Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpService extends Http {

  constructor (backend: XHRBackend, options: RequestOptions) {
    const token = sessionStorage.getItem('auth_token_condo'); // your custom token getter function here
    if (token !== null) {
      options.headers.set('x-auth-token', `${token}`);
    }
    options.headers.set('Accept', 'application/json');
    options.headers.set('Content-Type', 'application/json;charset=UTF-8');
    super(backend, options);
  }

  request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    const token = sessionStorage.getItem('auth_token_condo');
    if (typeof url === 'string') {
      // meaning we have to add the token to the options, not in url
      if (!options) {
        // let's make option object
        options = { headers: new Headers() };
      }
      if (token !== null) {
        options.headers.set('x-auth-token', `${token}`);
      }
    } else {
      // we have to add the token to the url object

      if (url.url.startsWith('//viacep.com.br/ws/')) {
        url.headers.delete('x-auth-token');
        url.headers.delete('Accept');
        url.headers.delete('Content-Type');
      } else {
        if (token !== null) {
          url.headers.set('x-auth-token', `${token}`);
        }
      }
    }
    return super.request(url, options).catch(this.catchAuthError(this));
  }

  private catchAuthError (self: HttpService) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      console.log(res);
      if (res.status === 401 || res.status === 403) {
        // if not authenticated
        console.log(res);
      }
      return Observable.throw(res);
    };
  }
}