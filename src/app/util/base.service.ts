import { Headers } from '@angular/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpResponse } from '../http';

@Injectable()
export class BaseService {

  httpResponse: HttpResponse;


  getBaseUrl() {
    return environment.BASE_URL_SERVICE + environment.PORT_SERVICE;
  }

  getHeader() {
    const contentHeaders = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json;charset=UTF-8');
    return contentHeaders;
  }

}
