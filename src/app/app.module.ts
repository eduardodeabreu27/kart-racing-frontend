import { ResultService } from './result/result.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

/* App Root */
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';

/* Feature Components */
import { ResultComponent } from './result/result.component';


/* Routing Module */
import { AppRoutingModule } from './app-routing.module';

/* Shared Service */
import { FormDataService } from './data/formData.service';
import { WorkflowService } from './workflow/workflow.service';
import { HttpModule, Http } from '@angular/http';
import { HttpService } from './util/http.service';

@NgModule({
    imports:      [ BrowserModule, 
                    FormsModule,
                    AppRoutingModule,
                    HttpModule,

                  ],
    providers:    [{ provide: FormDataService, useClass: FormDataService },
                   { provide: WorkflowService, useClass: WorkflowService },


                   { provide: Http, useClass: HttpService },
                    HttpService,
                    ResultService
                ],
    declarations: [ AppComponent, NavbarComponent, ResultComponent],
    bootstrap:    [ AppComponent ]
})

export class AppModule {}