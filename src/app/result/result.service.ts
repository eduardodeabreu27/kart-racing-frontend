import { HttpService } from '../util/http.service';
// Angular
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { BaseService } from '../util/base.service';
import { HttpResponse } from '../http';
import { Personal } from '../data/formData.model';
import { Result } from './result.model';
import { Position } from './position.model';


@Injectable()
export class ResultService extends BaseService {

    constructor(
        private http: HttpService,
       // private notificationService: NotificationService
    ) {
        super();
    }

   

    findResult(): Observable<Array<Result>> {

        console.log('Chamar resultado');
        return this.http.get(`${this.getBaseUrl()}/race/result`)
            .map(res => {

                this.httpResponse = res.json() as HttpResponse;

                if (this.httpResponse.responseCode === '200') {

                    return this.httpResponse.object as Observable<Result>

                } else {
                    console.log('Nao carregou');
                   // this.notificationService.info('Info', this.httpResponse.responseDescription);

                }

            })
            .catch((err) => {
                return Observable.throw(`Erro ao buscar os condominios. [ERROR: ` + err + `]`);
            });
    }


    getCheckout(): Observable<Array<Result>> {
        return this.http.get(`${this.getBaseUrl()}/pick-up/nfc/checkout`)
            .map(res => {

                this.httpResponse = res.json() as HttpResponse;

                if (this.httpResponse.responseCode === '200') {

                   // return this.httpResponse.object as Observable<Personal>

                } else {

                   // this.notificationService.info('Info', this.httpResponse.responseDescription);

                }

            })
            .catch((err) => {
                return Observable.throw(`Erro ao fazer checkout. [ERROR: ` + err + `]`);
            });
    }


}
