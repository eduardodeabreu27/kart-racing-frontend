import { Position } from './position.model';

export interface Result {

    totalTime: string;
    bestTurnTime: string;
    nameRacingDriverWinner: string;
    listResultRace: Array<Position>;

}
