

export interface Position {

    position: string;
    racingDriverCode: string;
    racingDriverName: string;
    quantityTurnCompleted: string;
    totalTime: string;
    bestTurnTime: string;
    averageSpeed: string;
    timeToWinner: string;
    arrivalTime: number;

}
