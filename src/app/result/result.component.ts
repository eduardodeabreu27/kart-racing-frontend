import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Result } from './result.model';
import { Position } from './position.model';
import { ResultService } from './result.service';
import { ISubscription } from 'rxjs/Subscription';


declare interface TableData {
    headerRow: string[];
  }

@Component ({
    selector: 'result'
    ,templateUrl: './result.component.html'
})

export class ResultComponent implements OnInit {
    title = 'Resultado da corrida';
    result: Result;
    form: any;
    subscription: ISubscription;

    public tableData: TableData;
    listStatus: Array<{ idStatus: string; status: string }> = [];
  

    resultRace: Result;
    listResult: Array<Object> = new Array<Object>();
    listPosition: Array<Position> = new Array<Position>();
  

    constructor(private router: Router,
               private service: ResultService ) {
    }

    ngOnInit() {
        this.tableData = {
            headerRow: ['Posição', 'Piloto', 'Voltas', 'Tempo']
          };
          this.findResult();
    }

    findResult() {
         this.subscription = this.service.findResult()
          .subscribe((result: any) => {
            if (result !== undefined && result !== null ) {
              this.resultRace = result
              for( let position of result.listResultRace){
                  this.listPosition.push(position);
              }
            }
          }, error => {
            console.log("Não foi possivel carregar o resultado");
          });
      }

}
