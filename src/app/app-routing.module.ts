import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResultComponent } from './result/result.component';

import { WorkflowGuard } from './workflow/workflow-guard.service';



export const appRoutes: Routes = [
    { path: 'result',  component: ResultComponent },
    { path: '',   redirectTo: '/result', pathMatch: 'full' },
    // 6th Route
    { path: '**', component: ResultComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true} )],
  exports: [RouterModule],
  providers: [WorkflowGuard]
})

export class AppRoutingModule {}