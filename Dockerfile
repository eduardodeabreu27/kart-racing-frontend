
### STAGE 1: Build ###

# We label our stage as 'builder'
FROM node:9-alpine as builder

COPY package.json package-lock.json ./

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm i && mkdir /ng-app && cp -R ./node_modules ./ng-app

WORKDIR /ng-app

COPY . .

## Build the angular app in production mode and store the artifacts in dist folder
#RUN $(npm bin)/ng build --prod
RUN $(npm bin)/ng build


### STAGE 2: Setup ###

FROM nginx:1.13.3-alpine

## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From 'builder' stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]


# FROM node:8.9.4

# RUN mkdir /usr/src/app

# WORKDIR /usr/src/app

# COPY package*.json /usr/src/app/

# RUN npm install

# COPY . /usr/src/app

# EXPOSE 4200

# CMD [ "npm", "start" ]


# FROM node:6.10.0

# RUN mkdir -p /usr/src/app WORKDIR /usr/src/app

# COPY package.json /usr/src/app/ RUN npm install --dev && npm cache clean

# COPY . /usr/src/app

# CMD [ "npm", "start" ]

